<?php

$data = json_decode(file_get_contents('data.json'), true);
if (file_exists('.' . $_SERVER['PATH_INFO'])) {
  return false;
}



else if (preg_match('/^\/([a-z_\-0-9]+)$/', $_SERVER['REQUEST_URI'], $match) && isset($data[$match[1]])) {
  error_log($_SERVER['REQUEST_URI'], 4);
  $html = file_get_contents('detail.html');

  foreach ($data[$match[1]] as $key => $value) {
    $html = str_replace("{{ $key }}", $value, $html);
  }

  echo $html;
}
else {
  error_log($_SERVER['REQUEST_URI'], 4);
  http_response_code(404);
  include_once '404.html';
}
